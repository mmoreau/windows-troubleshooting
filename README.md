# Windows Troubleshooting

Solves the problems that may be encountered by windows 

## Windows Update 

### Error Code - 0x8024a105

Put the codes in a .bat file (windows_update.bat) then execute the file with **administrative rights**.

```bash
net stop wuauserv
net stop cryptSvc
net stop bits
net stop msiserver
ren C:\Windows\SoftwareDistribution SoftwareDistribution.old
ren C:\Windows\System32\catroot2 Catroot2.old
net start wuauserv
net start cryptSvc
net start bits
net start msiserver
```

Reboot the computer

**Source** : https://answers.microsoft.com/en-us/windows/forum/windows_10-update/error-code-0x8024a105-windows-10-automatic-update/d6582ab4-e81c-4fe1-977f-84f0ae744f5d

---

## Fixed the bug of the black screen when changing the wallpaper on the desktop ##

1. Open powershell or cmd with administrator rights
2. Write this command : 

    ```
    slmgr -rearm
    ```

3. Reboot the computer for the changes to be made

---